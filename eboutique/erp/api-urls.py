from django.urls import path, include
from rest_framework import routers

from . import views


# Create router
router = routers.DefaultRouter()
router.register('product', views.ProductViewSet)
router.register('supplier', views.SupplierViewSet)

urlpatterns = [
    path('', include(router.urls), name='api')
]